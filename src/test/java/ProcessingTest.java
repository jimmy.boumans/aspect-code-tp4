import bot.Processing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class ProcessingTest {

    private Processing processing;
    private Processing processingMock;

    @BeforeEach
    void initializeObject() {
        processing = new Processing();
        processingMock = mock(Processing.class);
    }

    @Test
    void itShouldCallScanMethod() {
        processingMock.scan();
        verify(processingMock, times(1)).scan();
    }

}
