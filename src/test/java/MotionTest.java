import bot.Motion;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MotionTest {

    private Motion motion;
    int[][] board = {
            { 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 0 },
    };

    @BeforeEach
    void initializeObject() {
        motion = new Motion();
    }

    @Test
    void itShouldDisplayBoard() {
        motion.displayBoard(board);
    }

}
