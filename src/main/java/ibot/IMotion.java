package ibot;

public interface IMotion {
    public boolean move(int distanceInCm);

    public void rotate(int angleInDegree);
}