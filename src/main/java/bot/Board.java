package bot;

public class Board {
    private Cell[] row;
    private Cell[] line;

    /*private Cell[][] board = {
            {row},
            {line}
    };*/

    public Cell[] getRow() {
        return row;
    }

    public void setRow(Cell[] row) {
        this.row = row;
    }

    public Cell[] getLine() {
        return line;
    }

    public void setLine(Cell[] line) {
        this.line = line;
    }

    /*
    public Cell[][] getBoard() {
        return board;
    }

    public void setBoard(Cell[][] board) {
        this.board = board;
    }
    */

}
