package bot;

import ibot.IMotion;

public class Motion implements IMotion {

    private static final int BoardSizeX = 5;
    private static final int BoardSizeY = 5;
    private int[][] board = {};


    public boolean move(int distanceInCm) { return true;  }

    public void rotate(int angleInDegree) {    }


    public static void displayBoard(int[][] board) {
        for (int line = 0; line < BoardSizeX; line++) {
            for (int column = 0; column < BoardSizeY; column++) {
                System.out.print(board[line][column] + " ");
            }
            System.out.println();
        }
    }

    public void setLimitOfBoard(int[][] board){
        for (int line = 0; line < BoardSizeX; line++) {
            for (int column = 0; column < BoardSizeY; column++) {
                while(line == 0 || line == BoardSizeX || column == 0 || column == BoardSizeY) {
                    //TODO : Set la value de la cellule à -1
                }
            }
            System.out.println();
        }
    }
}
